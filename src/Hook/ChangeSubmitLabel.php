<?php

namespace Drupal\change_labels\Hook;

use Drupal\Core\Entity\ContentEntityFormInterface;
use Drupal\Core\Entity\Display\EntityFormDisplayInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Hook\Attribute\Hook;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Change labels hooks.
 */
class ChangeSubmitLabel {

  use StringTranslationTrait;

  /**
   * Implements hook_form_BASE_FORM_ID_alter().
   *
   * Case for entity_form_display_form.
   *
   * @param array $form
   *   Nested array of form elements that comprise the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param string $form_id
   *   String representing the name of the form itself.
   */
  #[Hook('form_entity_form_display_form_alter')]
  public function addSettingsForm(&$form, FormStateInterface $form_state, $form_id): void {
    /** @var \Drupal\Core\Entity\EntityForm $formObject */
    $formObject = $form_state->getFormObject();
    /** @var \Drupal\Core\Entity\Display\EntityFormDisplayInterface $entityFormDisplay */
    $entityFormDisplay = $formObject->getEntity();
    $form['change_labels'] = [
      '#type' => 'details',
      '#title' => $this->t('Change labels'),
    ];
    $form['change_labels']['submit_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Button label for "Submit"'),
      '#default_value' => $entityFormDisplay->getThirdPartySetting('change_labels', 'submit_label'),
    ];
    $form['change_labels']['submit_message'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Submit message replacement'),
      '#description' => $this->t('EXPERIMENTAL: setting a value here will suppress all status messages like "NODE_TYPE TITLE has been created" from node module.'),
      '#default_value' => $entityFormDisplay->getThirdPartySetting('change_labels', 'submit_message'),
    ];
    $form['#entity_builders'][] = [$this, 'saveThirdPartySettings'];
  }

  /**
   * Entity Builder Callback.
   *
   * @param string $entityType
   *   The entity type ID.
   * @param \Drupal\Core\Entity\Display\EntityFormDisplayInterface $entityFormDisplay
   *   The entity form display entity.
   * @param array $form
   *   Nested array of form elements that comprise the form.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The current state of the form.
   */
  public function saveThirdPartySettings($entityType, EntityFormDisplayInterface $entityFormDisplay, array &$form, FormStateInterface $formState): void {
    foreach (['submit_label', 'submit_message'] as $field) {
      if ($formState->getValue($field)) {
        $entityFormDisplay->setThirdPartySetting('change_labels', $field, $formState->getValue($field));
      }
      else {
        $entityFormDisplay->unsetThirdPartySetting('change_labels', $field);
      }
    }
  }

  /**
   * Implements hook_form_alter().
   *
   * @param array $form
   *   Nested array of form elements that comprise the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param string $form_id
   *   String representing the name of the form itself.
   */
  #[Hook('form_alter')]
  public function formAlter(&$form, FormStateInterface $form_state, $form_id): void {
    $formObject = $form_state->getFormObject();
    if ($formObject instanceof ContentEntityFormInterface && isset($form['actions']['submit']['#value'])) {
      $display = $formObject->getFormDisplay($form_state);
      if ($submitReplacement = $display->getThirdPartySetting('change_labels', 'submit_label')) {
        $form['actions']['submit']['#value'] = $submitReplacement;
      }
      if ($display->getThirdPartySetting('change_labels', 'submit_message')) {
        $form['actions']['submit']['#submit'][] = static::class . '::replaceMessage';
      }
    }
  }

  /**
   * Change message when a node is submitted.
   *
   * @param array $form
   *   Form.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   Form state.
   */
  public static function replaceMessage(array $form, FormStateInterface $formState): void {
    $formObject = $formState->getFormObject();
    if (!$formObject instanceof ContentEntityFormInterface) {
      return;
    }
    $display = $formObject->getFormDisplay($formState);
    if ($submitMessageReplacement = $display->getThirdPartySetting('change_labels', 'submit_message')) {
      // Remove all status messages.
      $messenger = \Drupal::messenger();
      $messenger->deleteByType($messenger::TYPE_STATUS);
      $messenger->addStatus($submitMessageReplacement);
    }
  }

}
