<?php

namespace Drupal\change_labels\Hook;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\NumberWidget;
use Drupal\Core\Field\WidgetInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Hook\Attribute\Hook;
use Drupal\Core\Render\Element;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Change labels hook.
 */
trait ChangeNumberField {

  use StringTranslationTrait;

  /**
   * Implements hook_field_widget_third_party_settings_form().
   *
   * @param \Drupal\Core\Field\WidgetInterface $plugin
   *   The instantiated field widget plugin.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $fieldDefinition
   *   The field definition.
   * @param string $form_mode
   *   The entity form mode.
   * @param array $form
   *   The (entire) configuration form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   Returns the form array to be built.
   */
  public function addSettingsChangeNumberField(WidgetInterface $plugin, FieldDefinitionInterface $fieldDefinition, $form_mode, array $form, FormStateInterface $form_state): array {
    $element = [];
    if (!is_a($plugin, NumberWidget::class)) {
      return $element;
    }
    $element['number_size'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Number field size'),
      '#default_value' => $plugin->getThirdPartySetting('change_labels', 'number_size'),
    ];
    return $element;
  }

  /**
   * Implements hook_field_widget_complete_form_alter().
   *
   * @param array $field_widget_complete_form
   *   The field widget form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $context
   *   An associative array containing the following key-value pairs:
   *     form: The form structure to which widgets are being attached.
   *     widget: The widget plugin instance.
   *     items: The field values, a FieldItemListInterface object.
   *     delta: The order of this item in the array of subelements.
   *     default: boolean. whether the form is to set default values.
   */
  #[Hook('field_widget_complete_form_alter')]
  public function alterFormChangeNumberField(&$field_widget_complete_form, FormStateInterface $form_state, $context): void {
    $setting = $context['widget']->getThirdPartySetting('change_labels', 'number_size');
    if ($setting) {
      $elements = &$field_widget_complete_form['widget'];
      foreach (Element::children($elements) as $child) {
        $elements[$child]['value']['#size'] = $setting;
      }
    }
  }

}
