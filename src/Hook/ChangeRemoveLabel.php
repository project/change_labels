<?php

namespace Drupal\change_labels\Hook;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\WidgetInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Hook\Attribute\Hook;
use Drupal\Core\Render\Element;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\file\Plugin\Field\FieldWidget\FileWidget;

/**
 * Change labels hooks.
 */
trait ChangeRemoveLabel {

  use StringTranslationTrait;

  /**
   * Implements hook_field_widget_third_party_settings_form().
   *
   * @param \Drupal\Core\Field\WidgetInterface $plugin
   *   The instantiated field widget plugin.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $fieldDefinition
   *   The field definition.
   * @param string $form_mode
   *   The entity form mode.
   * @param array $form
   *   The (entire) configuration form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   Returns the form array to be built.
   */
  public function addSettingsChangeRemoveLabel(WidgetInterface $plugin, FieldDefinitionInterface $fieldDefinition, $form_mode, array $form, FormStateInterface $form_state): array {
    $element = [];
    if (!($plugin instanceof FileWidget)) {
      return $element;
    }

    $element['remove_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Button label for "Remove"'),
      '#default_value' => $plugin->getThirdPartySetting('change_labels', 'remove_label'),
    ];
    return $element;
  }

  /**
   * Implements hook_field_widget_complete_form_alter().
   *
   * @param array $field_widget_complete_form
   *   The field widget form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $context
   *   An associative array containing the following key-value pairs:
   *     form: The form structure to which widgets are being attached.
   *     widget: The widget plugin instance.
   *     items: The field values, a FieldItemListInterface object.
   *     delta: The order of this item in the array of subelements.
   *     default: boolean. whether the form is to set default values.
   */
  #[Hook('field_widget_complete_form_alter')]
  public function alterFormChangeRemoveLabel(&$field_widget_complete_form, FormStateInterface $form_state, $context): void {
    $settings = $context['widget']->getThirdPartySetting('change_labels', 'remove_label');
    if ($settings) {
      $elements = &$field_widget_complete_form['widget'];
      foreach (Element::children($elements) as $child) {
        $elements[$child]['#change_label_remove'] = $settings;
        $elements[$child]['#process'][] = [
          static::class,
          'processChangeRemoveLabel',
        ];
      }
    }
  }

  /**
   * Process callback to swap default label for the configured one.
   *
   * @param array $element
   *   Render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $form
   *   Complete form.
   */
  public static function processChangeRemoveLabel($element, FormStateInterface $form_state, $form): array {
    if (isset($element['remove_button'])) {
      $element['remove_button']['#value'] = $element['#change_label_remove'];
    }
    return $element;
  }

}
