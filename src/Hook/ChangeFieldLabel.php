<?php

namespace Drupal\change_labels\Hook;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\WidgetInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Hook\Attribute\Hook;
use Drupal\Core\Render\Element;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Change labels hooks.
 *
 * @todo Change to a class instead of trait when multiple
 * implementations of a hook per module is supported.
 */
trait ChangeFieldLabel {

  use StringTranslationTrait;

  /**
   * Implements hook_field_widget_third_party_settings_form().
   *
   * @param \Drupal\Core\Field\WidgetInterface $plugin
   *   The instantiated field widget plugin.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $fieldDefinition
   *   The field definition.
   * @param string $form_mode
   *   The entity form mode.
   * @param array $form
   *   The (entire) configuration form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   Returns the form array to be built.
   */
  public function addSettingsFieldLabel(WidgetInterface $plugin, FieldDefinitionInterface $fieldDefinition, $form_mode, array $form, FormStateInterface $form_state): array {
    $element = [];
    $element['field_label_overwrite'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Overwrite field label'),
      '#description' => $this->t('Enter &lt;nolabel&gt; to remove the label.'),
      '#default_value' => $plugin->getThirdPartySetting('change_labels', 'field_label_overwrite'),
    ];
    return $element;
  }

  /**
   * Implements hook_field_widget_complete_form_alter().
   *
   * @param array $field_widget_complete_form
   *   The field widget form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $context
   *   An associative array containing the following key-value pairs:
   *     form: The form structure to which widgets are being attached.
   *     widget: The widget plugin instance.
   *     items: The field values, a FieldItemListInterface object.
   *     delta: The order of this item in the array of subelements.
   *     default: boolean. whether the form is to set default values.
   */
  #[Hook('field_widget_complete_form_alter')]
  public function alterFormFieldLabel(&$field_widget_complete_form, FormStateInterface $form_state, $context): void {
    $setting = $context['widget']->getThirdPartySetting('change_labels', 'field_label_overwrite');
    if ($setting) {
      $elements = &$field_widget_complete_form['widget'];
      $this->setNewLabel($elements, $setting);
      foreach (Element::children($elements) as $child) {
        $this->setNewLabel($elements[$child], $setting);
      }
    }
  }

  /**
   * Apply the label setting to an element.
   *
   * @param array $element
   *   Form array.
   * @param string $setting
   *   Option configured for field_label_overwrite.
   */
  protected function setNewLabel(&$element, $setting): void {
    foreach (Element::children($element) as $child) {
      $this->setNewLabel($element[$child], $setting);
    }
    if ($setting === '<nolabel>') {
      if (array_key_exists('#change_labels_title_display', $element)) {
        $element['#title_display'] = $element['#change_labels_title_display'];
      }
      else {
        $element['#title_display'] = 'invisible';
      }

      // Special case for datetime.
      if (isset($element['#type']) && $element['#type'] === 'datetime') {
        $element['#title'] = '';
      }

      if ($this->avoidProcessing($element)) {
        return;
      }
      $processCallback = static::class . '::processChangeFieldLabel';
      // Avoid adding the callback more than once.
      if (isset($element['#process']) && in_array($processCallback, $element['#process'])) {
        return;
      }
      $element['#process'][] = $processCallback;
      return;
    }
    $element['#title'] = $setting;
  }

  /**
   * Wether a "#process" callback should be added to the element or not.
   *
   * @param array $element
   *   Render array.
   */
  protected function avoidProcessing($element): bool {
    $types = ['address', 'datetime'];
    if (isset($element['#type']) && in_array($element['#type'], $types)) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Process callback to add a pre_render after the ones added by the theme.
   *
   * @param array $element
   *   Render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $form
   *   Complete form.
   */
  public static function processChangeFieldLabel($element, FormStateInterface $form_state, $form): array {
    $element['#pre_render'][] = [
      static::class,
      'overwriteLabel',
    ];
    return $element;
  }

  /**
   * Process callback to hide label when there is a details element.
   *
   * For example single file widgets.
   *
   * @param array $element
   *   Render array.
   */
  public static function overwriteLabel($element): array {
    if (isset($element['#theme_wrappers']['details'])) {
      $element['#theme_wrappers']['details']['#summary_attributes']['class'][] = 'visually-hidden';
    }

    // Case for the details wrapper of the address field.
    if (isset($element['#type']) && $element['#type'] === 'details') {
      $element['#summary_attributes']['class'][] = 'visually-hidden';
    }
    return $element;
  }

}
