<?php

namespace Drupal\change_labels\Hook;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\WidgetInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Hook\Attribute\Hook;
use Drupal\Core\Render\Element;
use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Change labels hooks.
 */
class ChangeAddAnotherLabel implements TrustedCallbackInterface {

  use StringTranslationTrait;
  use ChangeFieldLabel;
  use ChangeNumberField;
  use ChangeRemoveLabel;

  /**
   * Implements hook_field_widget_third_party_settings_form().
   *
   * @param \Drupal\Core\Field\WidgetInterface $plugin
   *   The instantiated field widget plugin.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $fieldDefinition
   *   The field definition.
   * @param string $form_mode
   *   The entity form mode.
   * @param array $form
   *   The (entire) configuration form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   Returns the form array to be built.
   */
  #[Hook('field_widget_third_party_settings_form')]
  public function addSettings(WidgetInterface $plugin, FieldDefinitionInterface $fieldDefinition, $form_mode, array $form, FormStateInterface $form_state): array {
    $element = [];

    // Fake two implementations of the same hook.
    // Remove when https://www.drupal.org/project/drupal/issues/3489707
    // Lands in stable.
    $element = array_merge($element, $this->addSettingsFieldLabel($plugin, $fieldDefinition, $form_mode, $form, $form_state));
    $element = array_merge($element, $this->addSettingsChangeNumberField($plugin, $fieldDefinition, $form_mode, $form, $form_state));
    $element = array_merge($element, $this->addSettingsChangeRemoveLabel($plugin, $fieldDefinition, $form_mode, $form, $form_state));

    if ($fieldDefinition->getFieldStorageDefinition()->getCardinality() === 1) {
      return $element;
    }

    $element['add_another'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Button label for "Add another"'),
      '#default_value' => $plugin->getThirdPartySetting('change_labels', 'add_another'),
    ];
    $element['hide_add_another'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hide "Add another"'),
      '#default_value' => $plugin->getThirdPartySetting('change_labels', 'hide_add_another'),
    ];
    $element['force_single_cardinality'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Force the widget to allow only one item'),
      '#default_value' => $plugin->getThirdPartySetting('change_labels', 'force_single_cardinality'),
    ];
    return $element;
  }

  /**
   * Implements hook_field_widget_complete_form_alter().
   *
   * @param array $field_widget_complete_form
   *   The field widget form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $context
   *   An associative array containing the following key-value pairs:
   *     form: The form structure to which widgets are being attached.
   *     widget: The widget plugin instance.
   *     items: The field values, a FieldItemListInterface object.
   *     delta: The order of this item in the array of subelements.
   *     default: boolean. whether the form is to set default values.
   */
  #[Hook('field_widget_complete_form_alter')]
  public function alterForm(&$field_widget_complete_form, FormStateInterface $form_state, $context): void {
    $addAnotherLabel = $context['widget']->getThirdPartySetting('change_labels', 'add_another');
    $elements = &$field_widget_complete_form['widget'];
    if ($addAnotherLabel && isset($elements['add_more'])) {
      $elements['add_more']['#value'] = $addAnotherLabel;
    }
    $hideAddAnother = $context['widget']->getThirdPartySetting('change_labels', 'hide_add_another');
    if ($hideAddAnother && isset($elements['add_more'])) {
      $elements['add_more']['#access'] = FALSE;
    }
    $singleCardinality = $context['widget']->getThirdPartySetting('change_labels', 'force_single_cardinality');
    if ($singleCardinality && isset($elements['#theme']) && $elements['#theme'] === 'field_multiple_value_form') {
      $elements['#cardinality'] = 1;
      $elements['#cardinality_multiple'] = FALSE;
      $children = Element::children($elements);
      foreach ($children as $key) {
        $elements[$key]['_weight']['#access'] = FALSE;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks(): array {
    // @todo move to ChangeFieldLabel class when is converted from being a trait.
    return [
      'overwriteLabel',
    ];
  }

}
