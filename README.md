# Change labels

Adds the ability to change strings in the interface with additional
context that the normal interface translation doesn't provide

## Features

* Change or hide field label on a per-widget basis.
* Change the "Add another item" per widget in multivalued fields.
* Replace the "Remove" button text for file and image widgets.
* Use a different label for the "Save" button of an entity form.

## Usage

Enable the module and change the settings for widgets in a form display mode.
For example: when you have team content type with team members you can change
the text to "Add another team member".

## Version 2 update

Early version 1 of the module had hook_event_dispatcher as a dependency.
It is not needed since 1.4.0. Hook classes from Drupal core are used instead.
The hook_event_dispatcher should be uninstalled if no other module is using it.
The dependency is still in the composer.json to avoid the module to disappear in
an update without being uninstalled first.

## Improvements and contributions

The module is open to add more possibilities for labels/texts to be changed.

## Notes

This project started as a replacement for the
[Custom add another module](https://www.drupal.org/project/custom_add_another)
since there is no Drupal 11 version and there is some not-so-flexible design
choices there like tying the storage configuration to the overridden label.
